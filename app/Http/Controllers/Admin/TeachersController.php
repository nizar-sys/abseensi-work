<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\EmployeeProfile;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Requests\Admin\RequestStoreTeacher;
use Illuminate\Support\Facades\Hash;

class TeachersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $employee_list = EmployeeProfile::orderByDesc('id');
        $employee_list->select(['uuid', 'user_id', 'employee_tier', 'employee_stats']);
        $employee_list = $employee_list->paginate(50);

        return view('admin.teachers.index', compact('employee_list'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("admin.teachers.add.index");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedPayload = $request->all();


        if ($request->hasFile('avatar')) {
            $fileName = time() . '.' . $request->avatar->extension();
            $validatedPayload['avatar'] = $fileName;

            // move file
            $request->avatar->move(public_path('uploads/images'), $fileName);
        }

        $user = User::create($validatedPayload);
        $user->employee()->create($validatedPayload);
        $user->personal()->create($validatedPayload);

        return redirect()->route('teachers.index')->with('success', 'Data berhasil ditambah.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $employee = EmployeeProfile::whereUuid($id)->first();

        if (is_null($employee)) {
            return redirect()->back()->with('error', 'Data tidak ditemukan.');
        }

        return view("admin.teachers.detail.detail-teacher", compact('employee'));
    }

    public function detail($id)
    {
        $employee = EmployeeProfile::whereUuid($id)->first();

        if (is_null($employee)) {
            return redirect()->back()->with('error', 'Data tidak ditemukan.');
        }

        $month_date = Carbon::parse($employee->join_date)->month . ' Bulan';

        return view("admin.teachers.detail.detail-employment", compact('employee', 'month_date'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = EmployeeProfile::whereUuid($id)->first()?->user;

        // with employee and profile
        $user = $user->load(['employee', 'personal']);

        return view("admin.teachers.edit.edit-teacher", compact('user'));
    }

    public function detailEdit($id)
    {
        $user = EmployeeProfile::whereUuid($id)->first()?->user;

        // with employee and profile
        $user = $user->load(['employee', 'personal']);

        return view("admin.teachers.edit.edit-employee", compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $uuid)
    {
        $user = EmployeeProfile::whereUuid($uuid)->first()?->user;

        // with employee and profile
        $user = $user->load(['employee', 'personal']);

        if (is_null($user)) {
            return response()->json([
                'success' => false,
                'message' => "Data tidak ditemukan.",
            ]);
        }

        $payloadUpdateUser = $request->only(['fullname', 'email', 'status', 'role']);
        $payloadUpdateEmployee = $request->only(['employee_tier', 'employee_stats', 'institution', 'join_date', 'stop_date', 'payroll_date']);
        $payloadUpdateProfile = $request->only(['nik', 'address', 'marriage', 'phone_number', 'birth_date', 'birth_place', 'gender', 'religion']);

        if ($request->hasFile('avatar')) {
            $fileName = time() . '.' . $request->avatar->extension();
            $payloadUpdateUser['avatar'] = $fileName;

            // move file
            $request->avatar->move(public_path('uploads/images'), $fileName);

            // delete old file
            $oldPath = public_path('/uploads/images/' . $user->avatar);
            if (file_exists($oldPath) && $user->avatar != 'avatar.png') {
                unlink($oldPath);
            }
        }

        $user->update($payloadUpdateUser);
        $user->employee()->update($payloadUpdateEmployee);
        $user->personal()->update($payloadUpdateProfile);

        return back()->with('success', 'Data berhasil diubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::whereUuid($id)->first();
        if (is_null($user)) {
            return response()->json([
                'success' => false,
                'message' => "Data gagal dihapus.",
                'isd' => $id,
            ]);
        }

        // delete old file
        $oldPath = public_path('/uploads/images/' . $user->avatar);
        if (file_exists($oldPath) && $user->avatar != 'avatar.png') {
            unlink($oldPath);
        }
        $user->delete();

        return response()->json([
            'success' => true,
            'message' => "Data berhasil dihapus.",
        ]);
    }
}
