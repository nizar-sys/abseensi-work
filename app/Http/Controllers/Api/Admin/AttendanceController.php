<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\RequestAttendance;
use App\Models\Attendance;
use Illuminate\Http\Request;

class AttendanceController extends Controller
{
    public function index()
    {
        $attendances = Attendance::orderByDesc('id');

        $attendances->select(['uuid', 'employee_id', 'attendance_date', 'attendance_time', 'attendance_status']);

        // load employee and user detail
        $attendances = $attendances->with(['employee', 'employee.user']);

        $attendances = $attendances->paginate(50);

        return response()->json([
            'success' => true,
            'data' => $attendances,
            'message' => 'Daftar data absensi.'
        ]);
    }

    public function show($uuidAttendance)
    {
        $attendance = Attendance::where('uuid', $uuidAttendance);

        // load employee and user detail
        $attendance = $attendance->with(['employee', 'employee.user']);

        $attendance = $attendance->firstOrFail();

        return response()->json([
            'success' => true,
            'data' => $attendance,
            'message' => 'Detail data absensi.'
        ]);
    }

    public function store(RequestAttendance $request)
    {
        $attendance = Attendance::create($request->validated());

        // load employee and user detail
        $attendance->load(['employee', 'employee.user']);

        return response()->json([
            'success' => true,
            'data' => $attendance,
            'message' => 'Data absensi berhasil ditambahkan.'
        ]);
    }

    public function update(Request $request, $uuidAttendance)
    {
        $attendance = Attendance::where('uuid', $uuidAttendance)->firstOrFail();

        $payloadUpdateAttendance = $request->only(['presence_status', 'presence_desc']);

        $attendance->update($payloadUpdateAttendance);

        // load employee and user detail
        $attendance->load(['employee', 'employee.user']);

        return response()->json([
            'success' => true,
            'data' => $attendance,
            'message' => 'Data absensi berhasil diperbarui.'
        ]);
    }

    public function destroy($uuidAttendance)
    {
        $attendance = Attendance::where('uuid', $uuidAttendance)->firstOrFail();

        $attendance->delete();

        return response()->json([
            'success' => true,
            'data' => $attendance,
            'message' => 'Data absensi berhasil dihapus.'
        ]);
    }
}
