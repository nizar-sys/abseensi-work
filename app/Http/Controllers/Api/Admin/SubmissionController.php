<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\RequestStoreOrUpdateSubmission;
use App\Models\Submission;
use Illuminate\Http\Request;

class SubmissionController extends Controller
{
    public function index()
    {

        $submissions = Submission::orderByDesc('id');

        $submissions->select(['uuid', 'employee_id', 'submission_type', 'start_timeoff', 'finish_timeoff', 'submission_desc', 'submission_file', 'submission_status']);

        // load employee and user detail
        $submissions = $submissions->with(['employee', 'employee.user']);

        $submissions = $submissions->paginate(50);

        return response()->json([
            'success' => true,
            'data' => $submissions,
            'message' => 'Daftar data pengajuan cuti.'
        ]);
    }

    public function store(RequestStoreOrUpdateSubmission $request)
    {
        $validatedPayload = $request->validated();

        if($request->hasFile('submission_file')){
            $fileName = time() . '.' . $request->submission_file->extension();
            $validatedPayload['submission_file'] = $fileName;

            // move file
            $request->submission_file->move(public_path('uploads/submissions'), $fileName);
        }

        $submission = Submission::create($validatedPayload);

        // load employee detail
        $submission = $submission->load(['employee', 'employee.user']);

        return response()->json([
            'success' => true,
            'data' => $submission,
            'message' => 'Berhasil tambah data pengajuan'
        ]);
    }

    public function show($uuidSubmission)
    {
        $submission = Submission::where('uuid', $uuidSubmission)->first();

        if(!$submission){
            return response()->json([
                'success' => false,
                'message' => 'Data pengajuan tidak ditemukan'
            ], 404);
        }

        // load employee detail
        $submission = $submission->load(['employee', 'employee.user']);

        return response()->json([
            'success' => true,
            'data' => $submission,
            'message' => 'Detail data pengajuan'
        ]);
    }

    public function update(RequestStoreOrUpdateSubmission $request, $uuidSubmission)
    {
        $submission = Submission::where('uuid', $uuidSubmission)->first();

        if(!$submission){
            return response()->json([
                'success' => false,
                'message' => 'Data pengajuan tidak ditemukan'
            ], 404);
        }

        $validatedPayload = $request->validated();

        if($request->hasFile('submission_file')){
            $fileName = time() . '.' . $request->submission_file->extension();
            $validatedPayload['submission_file'] = $fileName;

            // move file
            $request->submission_file->move(public_path('uploads/submissions'), $fileName);
        }

        $submission->update($validatedPayload);

        // load employee detail
        $submission = $submission->load(['employee', 'employee.user']);

        return response()->json([
            'success' => true,
            'data' => $submission,
            'message' => 'Berhasil update data pengajuan'
        ]);
    }

    public function destroy($uuidSubmission)
    {
        $submission = Submission::where('uuid', $uuidSubmission)->first();

        if(!$submission){
            return response()->json([
                'success' => false,
                'message' => 'Data pengajuan tidak ditemukan'
            ], 404);
        }

        // delete file
        if($submission->submission_file){
            $filePath = public_path('uploads/submissions/' . $submission->submission_file);
            if(file_exists($filePath)){
                unlink($filePath);
            }
        }

        $submission->delete();

        return response()->json([
            'success' => true,
            'message' => 'Berhasil hapus data pengajuan'
        ]);
    }
}
