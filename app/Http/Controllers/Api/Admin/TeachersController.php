<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\RequestStoreTeacher;
use App\Models\EmployeeProfile;
use App\Models\User;
use Illuminate\Http\Request;

class TeachersController extends Controller
{
    public function index()
    {
        $employee_list = EmployeeProfile::orderByDesc('id')->with(['user:id,uuid,fullname,email,role,avatar', 'personal']);

        $employee_list->select(['id', 'uuid', 'user_id', 'employee_tier', 'employee_stats']);
        $employee_list = $employee_list->paginate(50);

        return response()->json([
            'success' => true,
            'data' => $employee_list,
            'message' => "Teachers list",
        ]);
    }

    public function store(RequestStoreTeacher $request)
    {
        $validatedPayload = $request->validated();


        if ($request->hasFile('avatar')) {
            $fileName = time() . '.' . $request->avatar->extension();
            $validatedPayload['avatar'] = $fileName;

            // move file
            $request->avatar->move(public_path('uploads/images'), $fileName);
        }

        $user = User::create($validatedPayload);
        $user->employee()->create($validatedPayload);
        $user->personal()->create($validatedPayload);

        // with employee and profile
        $user = $user->load(['employee', 'personal']);

        return response()->json([
            'success' => true,
            'data' => $user,
            'message' => 'Data berhasil ditambah'
        ]);
    }

    public function update(Request $request, $uuid)
    {
        $user = EmployeeProfile::whereUuid($uuid)->first()?->user;

        // with employee and profile
        $user = $user->load(['employee', 'personal']);

        if (is_null($user)) {
            return response()->json([
                'success' => false,
                'message' => "Data tidak ditemukan.",
            ]);
        }

        $payloadUpdateUser = $request->only(['fullname', 'email', 'status', 'role']);
        $payloadUpdateEmployee = $request->only(['employee_tier', 'employee_stats', 'institution', 'join_date', 'stop_date', 'payroll_date']);
        $payloadUpdateProfile = $request->only(['nik', 'address', 'marriage', 'phone_number', 'birth_date', 'birth_place', 'gender', 'religion']);

        if ($request->hasFile('avatar')) {
            $fileName = time() . '.' . $request->avatar->extension();
            $payloadUpdateUser['avatar'] = $fileName;

            // move file
            $request->avatar->move(public_path('uploads/images'), $fileName);

            // delete old file
            $oldPath = public_path('/uploads/images/' . $user->avatar);
            if (file_exists($oldPath) && $user->avatar != 'avatar.png') {
                unlink($oldPath);
            }
        }

        $user->update($payloadUpdateUser);
        $user->employee()->update($payloadUpdateEmployee);
        $user->personal()->update($payloadUpdateProfile);

        return response()->json([
            'success' => true,
            'data' => $user,
            'message' => "Data berhasil diubah.",
        ]);
    }

    public function destroy($id)
    {
        $user = EmployeeProfile::whereUuid($id)->first()?->user;

        if (is_null($user)) {
            return response()->json([
                'success' => false,
                'message' => "Data gagal dihapus.",
                'isd' => $id,
            ]);
        }

        // delete old file
        $oldPath = public_path('/uploads/images/' . $user->avatar);
        if (file_exists($oldPath) && $user->avatar != 'avatar.png') {
            unlink($oldPath);
        }
        $user->delete();

        return response()->json([
            'success' => true,
            'message' => "Data berhasil dihapus.",
        ]);
    }

    public function show($uuid)
    {
        $user = EmployeeProfile::whereUuid($uuid)->first()?->user;

        // with employee and profile
        $user = $user->load(['employee', 'personal']);

        if (is_null($user)) {
            return response()->json([
                'success' => false,
                'message' => "Data tidak ditemukan.",
                'isd' => $uuid,
            ]);
        }

        return response()->json([
            'success' => true,
            'data' => $user,
            'message' => "Data ditemukan.",
        ]);
    }
}
