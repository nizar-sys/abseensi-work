<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class RequestStoreTeacher extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        $rules = [
            'fullname' => 'required|max:100',
            'email' => 'required|email|max:30|unique:users,email,' . $this->id,
            'password' => 'max:30|',
            'role' => 'required|max:20|in:admin,tu,teacher',
            'nik' => 'required|max:15|unique:personal_profiles,nik,' . $this->id,
            'address' => 'required',
            'marriage' => 'required',
            'phone_number' => 'required|numeric',
            'birth_date' => 'required|date',
            'birth_place' => 'required',
            'gender' => 'required',
            'religion' => 'required',
            'employee_tier' => 'required',
            'employee_stats' => 'required',
            'institution' => 'required',
            'join_date' => 'required|date',
            'stop_date' => 'nullable|date',
            'payroll_date' => 'required|date',
        ];

        if($this->isMethod('POST')){
            $rules['password'] = 'required|min:6';
        }

        return $rules;
    }

    public function messages()
    {
        return [
            'fullname.required' => 'Nama lengkap harus diisi',
            'fullname.max' => 'Nama lengkap maksimal 100 karakter',
            'email.required' => 'Email harus diisi',
            'email.email' => 'Format email tidak valid',
            'email.max' => 'Email maksimal 30 karakter',
            'password.required' => 'Password harus diisi',
            'password.max' => 'Password maksimal 30 karakter',
            'role.required' => 'Peran harus diisi',
            'role.max' => 'Peran maksimal 20 karakter',
            'role.in' => 'Peran harus berupa admin, tu, atau teacher',
            'nik.required' => 'NIK harus diisi',
            'nik.max' => 'NIK maksimal 15 karakter',
            'address.required' => 'Alamat harus diisi',
            'marriage.required' => 'Status pernikahan harus diisi',
            'phone_number.required' => 'Nomor telepon harus diisi',
            'phone_number.numeric' => 'Nomor telepon harus berupa angka',
            'birth_date.required' => 'Tanggal lahir harus diisi',
            'birth_date.date' => 'Format tanggal lahir tidak valid',
            'birth_place.required' => 'Tempat lahir harus diisi',
            'gender.required' => 'Jenis kelamin harus diisi',
            'religion.required' => 'Agama harus diisi',
            'employee_tier.required' => 'Tingkatan karyawan harus diisi',
            'employee_stats.required' => 'Status karyawan harus diisi',
            'institution.required' => 'Institusi harus diisi',
            'join_date.required' => 'Tanggal bergabung harus diisi',
            'join_date.date' => 'Format tanggal bergabung tidak valid',
            'stop_date.date' => 'Format tanggal berhenti tidak valid',
            'payroll_date.required' => 'Tanggal gaji harus diisi',
            'payroll_date.date' => 'Format tanggal gaji tidak valid',
        ];
    }
}
