<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RequestAttendance extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        $rules = [
            'employee_id' => 'required|exists:employee_profiles,id',
            'presence_date' => 'required|date',
            'presence_status' => 'required|string|max:50',
            'presence_desc' => 'required|string|max:50',
            'clock_in' => 'required|date_format:H:i:s',
            'clock_out' => 'required|date_format:H:i:s',
            'location_in' => 'required',
            'location_out' => 'required',
        ];

        if ($this->getMethod() === 'POST') {
            $rules['presence_date'] .= '|unique:attendances,presence_date,NULL,id,employee_id,' . $this->employee_id;
        }

        return $rules;
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function messages()
    {
        return [
            'employee_id.required' => 'ID Karyawan tidak boleh kosong.',
            'employee_id.exists' => 'ID Karyawan tidak ditemukan.',
            'presence_date.required' => 'Tanggal tidak boleh kosong.',
            'presence_date.date' => 'Tanggal tidak valid.',
            'presence_date.unique' => 'Tanggal sudah ada.',
            'presence_status.required' => 'Status tidak boleh kosong.',
            'presence_status.string' => 'Status harus berupa string.',
            'presence_status.max' => 'Status maksimal 50 karakter.',
            'presence_desc.required' => 'Keterangan tidak boleh kosong.',
            'presence_desc.string' => 'Keterangan harus berupa string.',
            'presence_desc.max' => 'Keterangan maksimal 50 karakter.',
            'clock_in.required' => 'Jam masuk tidak boleh kosong.',
            'clock_in.date_format' => 'Jam masuk tidak valid.',
            'clock_out.required' => 'Jam keluar tidak boleh kosong.',
            'clock_out.date_format' => 'Jam keluar tidak valid.',
            'location_in.required' => 'Lokasi masuk tidak boleh kosong.',
            'location_in.string' => 'Lokasi masuk harus berupa string.',
            'location_in.max' => 'Lokasi masuk maksimal 50 karakter.',
            'location_out.required' => 'Lokasi keluar tidak boleh kosong.',
            'location_out.string' => 'Lokasi keluar harus berupa string.',
            'location_out.max' => 'Lokasi keluar maksimal 50 karakter.',
        ];
    }
}
