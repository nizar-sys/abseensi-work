<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RequestStoreOrUpdateSubmission extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        $rules = [
            'employee_id' => 'required|integer|exists:employee_profiles,id',
            'submission_type' => 'required|string',
            'start_timeoff' => 'required|date',
            'finish_timeoff' => 'required|date|after:start_timeoff',
            'submission_desc' => 'required|string',
            'submission_file' => 'required|file',
        ];

        if($this->isMethod('put')){
            $rules['submission_file'] = 'nullable|file';
        }

        return $rules;
    }

    public function messages()
    {
        return [
            'submission_type.required' => 'Tipe pengajuan harus diisi',
            'submission_type.string' => 'Tipe pengajuan harus berupa string',
            'start_timeoff.required' => 'Waktu mulai harus diisi',
            'start_timeoff.date' => 'Format waktu mulai tidak valid',
            'finish_timeoff.required' => 'Waktu selesai harus diisi',
            'finish_timeoff.date' => 'Format waktu selesai tidak valid',
            'finish_timeoff.after' => 'Waktu selesai harus setelah waktu mulai',
            'submission_desc.required' => 'Deskripsi pengajuan harus diisi',
            'submission_desc.string' => 'Deskripsi pengajuan harus berupa string',
            'submission_file.required' => 'File pengajuan harus diisi',
            'submission_file.file' => 'File pengajuan harus berupa file',
        ];
    }

}
