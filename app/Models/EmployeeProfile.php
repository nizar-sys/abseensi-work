<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class EmployeeProfile extends Model
{
    use HasFactory;

    protected $fillable = [
        'uuid',
        'user_id',
        'employee_tier',
        'employee_stats',
        'institution',
        'join_date',
        'stop_date',
        'payroll_date',
    ];

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            $model->uuid = Str::uuid();
            $model->created_at = now();
        });

        static::updating(function ($model) {
            $model->updated_at = now();
        });
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function personal()
    {
        return $this->belongsTo(PersonalProfile::class, 'user_id', 'id');
    }

    public function submissions()
    {
        return $this->hasMany(Submission::class, 'employee_id', 'id');
    }
}
