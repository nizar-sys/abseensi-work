<?
namespace App\Http\Traits;

trait ResponseJson{

    public function responseSuccess($code = 200, $data = null, $message = 'Success'){
        return response()->json([
            'response' => 'success',
            'data' => $data,
            'message' => $message,
        ], $code);
    }

    public function responseError($data = null, $message = 'Failed'){
        return response()->json([
            'response' => 'error',
            'data' => $data,
            'message' => $message,
        ]);
    }
}