<?php

namespace Database\Seeders;

use App\Models\EmployeeProfile;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\support\Str;

class EmployeeProfileSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $employee_profiles = [
            [
                'uuid' => Str::uuid(),
                'user_id' => 1,
                'employee_tier' => 'administrator',
                'employee_stats' => 'permanen',
                'institution' => 'SMK WIKRAMA BOGOR',
                'join_date' => date('Y-m-d'),
                'stop_date' => null,
                'payroll_date' => date('Y-m-d'),
            ],
            [
                'uuid' => Str::uuid(),
                'user_id' => 2,
                'employee_tier' => 'Tata Usaha',
                'employee_stats' => 'kontrak',
                'institution' => 'SMK WIKRAMA BOGOR',
                'join_date' => date('Y-m-d'),
                'stop_date' => null,
                'payroll_date' => date('Y-m-d'),
            ],
            [
                'uuid' => Str::uuid(),
                'user_id' => 3,
                'employee_tier' => 'Guru',
                'employee_stats' => 'percobaan',
                'institution' => 'SMK WIKRAMA BOGOR',
                'join_date' => date('Y-m-d'),
                'stop_date' => null,
                'payroll_date' => date('Y-m-d'),
            ],
        ];

        EmployeeProfile::insert($employee_profiles);
    }
}
