<?php

namespace Database\Seeders;

use App\Models\PersonalProfile;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class PersonalProfileSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $personal_profiles = [
            [
                'uuid' => Str::uuid(),
                'user_id' => 1,
                'nik' => '23982342749238',
                'address' => 'adjaksdnad',
                'marriage' => 'adjaksdnad',
                'phone_number' => '08989739483',
                'birth_date' => date('Y-m-d'),
                'birth_place' => 'bogor',
                'gender' => 'male',
                'religion' => 'Islam',
            ],
            [
                'uuid' => Str::uuid(),
                'user_id' => 2,
                'nik' => '2334542749238',
                'address' => 'adjaksdnad',
                'marriage' => 'adjaksdnad',
                'phone_number' => '08989739483',
                'birth_date' => date('Y-m-d'),
                'birth_place' => 'bogor',
                'gender' => 'male',
                'religion' => 'Islam',
            ],
            [
                'uuid' => Str::uuid(),
                'user_id' => 3,
                'nik' => '239342749238',
                'address' => 'adjaksdnad',
                'marriage' => 'adjaksdnad',
                'phone_number' => '08989739483',
                'birth_date' => date('Y-m-d'),
                'birth_place' => 'bogor',
                'gender' => 'male',
                'religion' => 'Islam',
            ],
        ];
        PersonalProfile::insert($personal_profiles);
    }
}
