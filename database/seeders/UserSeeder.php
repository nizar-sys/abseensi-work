<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
            [
                'uuid' => Str::uuid(),
                'fullname' => 'admin',
                'email' => 'admin@mail.com',
                'email_verified_at' => date('Y-m-d H:i:s'),
                'password' => Hash::make('password'),
                'role' => 'admin',
                'status' => 'active',
                'created_at' => date(now()),
            ],
            [
                'uuid' => Str::uuid(),
                'fullname' => 'tata usaha',
                'email' => 'tu@mail.com',
                'email_verified_at' => date('Y-m-d H:i:s'),
                'password' => Hash::make('password'),
                'role' => 'tu',
                'status' => 'active',
                'created_at' => date(now()),
            ],
            [
                'uuid' => Str::uuid(),
                'fullname' => 'teacher',
                'email' => 'teacher@mail.com',
                'email_verified_at' => date('Y-m-d H:i:s'),
                'password' => Hash::make('password'),
                'role' => 'teacher',
                'status' => 'active',
                'created_at' => date(now()),
            ],
        ];

        User::insert($users);
    }
}
