$.ajaxSetup({
   headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
   }
});

$(document).ready(function () {
   $('#datatablesSimple').DataTable();
   $('.delete-employee').click((e) => {
      e.preventDefault()
      var data_uuid = $(e.target).data('id');
      if(data_uuid){
         Swal.fire({
            title: 'Apakah anda yakin?',
            text: "Data yang dihapus tidak dapat dikembalikan!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ya, hapus!'
         }).then((result) => {
            $.ajax({
               url: `${deleteUrl + '/' + data_uuid}`,
               type: 'DELETE',
               dataType: 'json',
               success: function (res) {
                  if (res.success) {
                     Swal.fire(
                        'Terhapus!',
                        'Berhasil hapus data.',
                        'success'
                     ).then(function () {
                        window.location.reload();
                     });
                  } else {
                     Swal.fire(
                        'Gagal!',
                        'Data gagal dihapus.',
                        'error'
                     );
                  }
               }
            });
         });
      }
   });
});