$(document).ready(() => {
   moment.locale('id')
   $('.dateLocalTerlambat').map((i, date) => {
      // $(date).html(moment.date($(date)))
      var dateLocal = moment($(date).html().replace(/\s/g, '')).format('[Terlambat : ]DD MMMM YYYY')
      $(date).html(dateLocal)
   })

   $('.dateLocalTidakHadir').map((i, date) => {
      // $(date).html(moment.date($(date)))
      var dateLocal = moment($(date).html().replace(/\s/g, '')).format('[Tidak Hadir : ]DD MMMM YYYY')
      $(date).html(dateLocal)
   })

   $('.dateLocalCuti').map((i, date) => {
      // $(date).html(moment.date($(date)))
      var dateLocal = moment($(date).html().replace(/\s/g, '')).format('[Cuti : ]DD MMMM YYYY')
      $(date).html(dateLocal)
   })

   $('.dateLocalDinasLuar').map((i, date) => {
      // $(date).html(moment.date($(date)))
      var dateLocal = moment($(date).html().replace(/\s/g, '')).format('[Dinas Luar : ]DD MMMM YYYY')
      $(date).html(dateLocal)
   })

   $('.dayLocal').map((i, day) => {
      var dayLocal = moment($(day).html().replace(/\s/g, '')).format('dddd')
      $(day).html(dayLocal)
   })

   $('.dateLocal').map((i, date) => {
      // $(date).html(moment.date($(date)))
      var dateLocal = moment($(date).html().replace(/\s/g, '')).format('DD MMMM YYYY')
      $(date).html(dateLocal)
   })
});

var barPermanen = document.getElementById("bar-permanen");
var progressbarPermanen = barPermanen