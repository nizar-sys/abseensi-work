@php
    $routeActive = Route::currentRouteName();
@endphp

<ul class="list-group mt-3">
    <a href="{{ route('teachers.show', ['teacher' => $employee->uuid]) }}"
        class="list-group-item {{ $routeActive == 'teachers.show' ? 'act-listgroup' : 'listgroup-container' }}">Data
        Diri</a>
    <a href="{{ route('teachers.detail', ['uuid' => $employee->uuid]) }}"
        class=" list-group-item {{ $routeActive == 'teachers.detail' ? 'act-listgroup' : 'listgroup-container' }}">Data
        Kepegawaian</a>
</ul>
