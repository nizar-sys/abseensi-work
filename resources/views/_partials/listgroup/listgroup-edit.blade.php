@php
    $routeActive = Route::currentRouteName();
@endphp


<ul class="list-group mt-3">
    <a href="{{ route('teachers.edit', ['teacher' => $user->employee->uuid]) }}"
        class="list-group-item {{ $routeActive == 'teachers.edit' ? 'act-listgroup' : 'listgroup-container' }}">Data
        Diri</a>
    <a href="{{ route('teachers.detail-edit', ['uuid' => $user->employee->uuid]) }}"
        class="list-group-item {{ $routeActive == 'teachers.detail-edit' ? 'act-listgroup' : 'listgroup-container' }}">Data
        Kepegawaian</a>
</ul>
