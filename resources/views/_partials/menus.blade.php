@php
    $routeActive = Route::currentRouteName();
@endphp

<!-- Nav Item - Dashboard -->
<li class="nav-item cont-border mt-5 mb-3">
    <a class="nav-link btnSide {{ $routeActive == 'home' ? 'btnAct' : '' }}" href="{{ route('home', []) }}">

        <x-icon type="dashboard-icon" />

        <span class="side-text">Dashboard</span>
    </a>
</li>

<!-- Nav Item - Pages Collapse Menu -->
<li class="nav-item mb-3 cont-border">
    <a class="nav-link btnSide collapsed {{ $routeActive == 'teachers.index' || $routeActive == 'presences.index' || $routeActive == 'timeoff.index' ? 'btnAct' : '' }}"
        href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">

        <x-icon type="guru-icon" />

        <span class="text-sidebar side-text2">Guru</span>
    </a>
    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
        <div class="bg-dropdown-sidebar collapse-inner rounded" style="background-color: #15548f">
            <a class="active-dropdown {{ $routeActive == 'teachers.index' ? 'act-dropdown' : '' }}"
                href="{{ route('teachers.index') }}">Daftar Guru</a>
            <a class="active-dropdown {{ $routeActive == 'presences.index' ? 'act-dropdown' : '' }} my-2"
                href="{{ route('presences.index') }}">Kehadiran</a>
            <a class="active-dropdown {{ $routeActive == 'timeoff.index' ? 'act-dropdown' : '' }}"
                href="{{ route('timeoff.index') }}">Time Off</a>
        </div>
    </div>
</li>

<li class="nav-item cont-border">
    <a class="nav-link btnSide" href="#">

        <x-icon type="pengaturan-icon" />
        <span class="side-text">Pengaturan</span>
    </a>
</li>
