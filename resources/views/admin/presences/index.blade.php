@extends('layouts.master')
@section('title', 'SIM Absensi Wikrama | Presences')

@section('css')
    @vite('resources/css/admin/presences/style.css')
@endsection

@section('content')
    <div id="layoutSidenav_content">
        <main>
            <div class="container-fluid px-4">
                <div class="row">
                    <div class="col">
                        <div class="card">
                            <div class="row">

                                <div class="col-md-2 col-sm-2 col-xl-3 borderDas ">
                                    <p class="labeldasKehadiran" style="padding-left: 20px; margin-top: 20px;">Data Yang
                                        Ditampilkan Adalah Data Real Time</p>
                                    <p class="fontdasKehadiran2 dateLocalToday"> <?= date('d F Y') ?></p>
                                </div>
                                <div class="col-md-5 col-sm-1 col-lg-6 col-xl-1 borderDas text-center">
                                    <p class="labeldasKehadiran">Jumlah</p>
                                    <p class="valuedasKehadiran">100</p>
                                    <p class="textkehadiran">Pegawai</p>
                                </div>
                                <div class="col-md-5 col-sm-1 col-lg-6 col-xl-1 borderDas text-center">
                                    <p class="labeldasKehadiran">Tepat Waktu</p>
                                    <p class="valuedasKehadiran">65</p>
                                    <p class="textkehadiran">Pegawai</p>
                                </div>
                                <div class="col-md-5 col-sm-1 col-lg-6 col-xl-1 borderDas text-center">
                                    <p class="labeldasKehadiran">Terlambat</p>
                                    <p class="valuedasKehadiran">5</p>
                                    <p class="textkehadiran">Pegawai</p>
                                </div>
                                <div class="col-md-5 col-sm-1 col-lg-6 col-xl-1 borderDas text-center">
                                    <p class="labeldasKehadiran">Time Off</p>
                                    <p class="valuedasKehadiran">10</p>
                                    <p class="textkehadiran">Pegawai</p>
                                </div>
                                <div class="col-md-5 col-sm-1 col-lg-6 col-xl-1 borderDas text-center">
                                    <p class="labeldasKehadiran">Tidak Hadir</p>
                                    <p class="valuedasKehadiran">7</p>
                                    <p class="textkehadiran">Pegawai</p>
                                </div>
                                <div class="col-md-5 col-sm-1 col-lg-6 col-xl-1 borderDas text-center">
                                    <p class="labeldasKehadiran">Tidak Absen</p>
                                    <p class="valuedasKehadiran">13</p>
                                    <p class="textkehadiran">Pegawai</p>
                                </div>
                                <div class="col-md-3 col-sm-2 col-lg-6 col-xl-3 text-center">
                                    <a href="{{ route('mini-dashboard') }}"><img
                                            src={{ asset('/master/img/icon-cursor.png') }} alt="cursor"
                                            class="imgDasKehadiran">
                                        <p class="text-detail">Lihat Semua</p>
                                    </a>
                                </div>
                            </div>
                        </div>

                        <div class="d-flex mt-2">
                            <div class="mr-auto">

                            </div>

                            <div class="ml-auto">
                                <small class="data-tanggal">Data Tanggal : </small>

                                <label for="" style="padding-top: 5px;">
                                    <input type="date" id="min" name="min">
                                </label>
                                <small style="color: #AAB2BB;">-</small>
                                <label for="" style="padding-right: 10px; padding-top: 5px;">
                                    <input type="date" id="max" name="max">
                                </label>
                                <button class="btn btn-primary btnExp eksportdata">Eksport Data</button>
                            </div>
                        </div>

                        <div class="card mt-4 mb-4">
                            <div class="card-header">
                                <i class="fas fa-table me-1"></i>
                                Daftar Kehadiran
                            </div>

                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-hover" id="datatablesSimple">
                                        <thead>
                                            <tr>
                                                <th style="width: 10px;"></th>
                                                <th>Nama Pegawai</th>
                                                <th>Tanggal</th>
                                                <th>Shift</th>
                                                <th>Jadwal Kerja</th>
                                                <th>Status</th>
                                                <th>Jam Masuk</th>
                                                <th>Jam Keluar</th>
                                                <th></th>
                                            </tr>
                                        </thead>

                                        <tbody>
                                            @foreach ($attendances as $attendance)
                                                <tr>
                                                    <td><img src={{ asset('/master/img/Ellipse1.png') }}
                                                            class="rounded-circle" style="width:65px"></td>
                                                    <td>
                                                        <div class="col">Xander Mount</div>
                                                        <div class="col"><small>12007787</small></div>
                                                    </td>
                                                    <td class="dateLocalDay"><?= date('d F Y') ?></td>
                                                    <td>N</td>
                                                    <td>08.00 - 17.00</td>
                                                    <td>
                                                        <span class="badge bg-merah">Cuti</span>
                                                    </td>
                                                    <td><?= date('h:i') ?></td>
                                                    <td>17:00</td>
                                                    <td><a href="{{ route('presences.show', ['presences']) }}">
                                                            <x-icon type="icon-cursor" />
                                                        </a></td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
    </div>
@endsection

@section('script')
    @vite('resources/js/admin/presences/script.js')
    @vite('resources/js/admin/sweetalert/sweetalert2.js')
    @vite('resources/js/admin/sweetalert/swalscript.js')
@endsection
