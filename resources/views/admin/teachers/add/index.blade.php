@extends('layouts.master')
@section('title', 'SIM Absensi Wikrama | Add Teacher')

@section('css')
    @vite('resources/css/admin/teachers/add/wizard.css')
    @vite('resources/css/admin/teachers/add/style.css')
@endsection

@section('content')
    <form method="POST" action="{{ route('teachers.store') }}">
        @csrf
        <div class="container">
            <div id="app">
                <step-navigation :steps="steps" :currentstep="currentstep">
                </step-navigation>

                {{-- Halaman Pertama (DATA DIRI) --}}
                <div v-show="currentstep == 1">
                    <div class="card">
                        <div class="container-fluid mt-3">
                            <div class="row">
                                <div class="col-md-4">
                                    <!-- CARD -->
                                    <div class="card bg-cardGradient mt-4">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="d-flex justify-content-center">
                                                    <x-icon type="icon-datadiri" />

                                                </div>
                                            </div>
                                        </div>
                                        <!-- CARD BODY -->
                                        <div class="card-body">
                                            <h5 class="card-title addTitle-card">Data Diri</h5>
                                            <h5 class="card-text addBody-card">Silakan lengkapi data pribadi Anda dengan
                                                baik baik dan sesuai dengan data yang ada</h5>
                                        </div>
                                    </div>
                                    <div class="page text-center">
                                        <span class="dotActive"></span>
                                        <span class="dot1"></span>
                                        <span class="dot1"></span>
                                    </div>

                                </div>
                                <div class="col-md-8">
                                    <div class="row">
                                        <div class="col-md-6 mt-3">
                                            <label class="nama labelAdd" id="nama-lbl" for="fullname">Nama</label><br>
                                            <input type="text" class="input-tambah form-control nama" name="fullname"
                                                id="nama" placeholder="Nama Lengkap" required>
                                        </div>
                                        <div class="col-md-6 mt-3">
                                            <label class="nik labelAdd" id="nik-lbl" for="nik">NIK</label><br>
                                            <input type="number" class="input-tambah form-control" name="nik"
                                                id="nik" placeholder="Masukan NIK pegawai" style="width: 100%;" required>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6 mt-3">
                                            <label for="birth_place" class="tempatlahir labelAdd" id="tempatlahir-lbl" for="nama">Tempat
                                                Lahir</label><br>
                                            <input type="text" class="input-tambah form-control" name="birth_place"
                                                id="birth_place" placeholder="Masukan tempat lahir " required>

                                        </div>
                                        <div class="col-md-6 mt-3">
                                            <label for="birth_date" class="tgllahir labelAdd" id="tgllahir-lbl" for="tgllahir">Tanggal
                                                Lahir</label><br>
                                            <input type="date" class="input-tambah form-control" name="birth_date"
                                                id="birth_date" placeholder="Masukan tanggal lahir Anda" required>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6 mt-3">
                                            <label class="alamat labelAdd" id="alamat-lbl" for="address">Alamat
                                                :</label><br>
                                            <input type="text" class="input-tambah form-control" name="address"
                                                id="alamat" placeholder="Masukan alamat pegawai" required>
                                        </div>
                                        <div class="col-md-6 mt-3">
                                            <label class="jenisKelamin labelAdd" id="jenisKelamin-lbl"
                                                for="gender">Jenis
                                                Kelamin :</label><br>
                                            <select name="gender" id="jenisKelamin-lbl"
                                                class="inputPegawai form-control" aria-placeholder="Pilih Jenis Kelamin" required>
                                                <option class="optKelamin" value=""> Pilih jenis kelamin pegawai</option>
                                                <option value="male" class="optKelamin"> Laki-laki</option>
                                                <option value="female" class="optKelamin"> Perempuan</option>
                                            </select>

                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6 mt-3">
                                            <label class="agama labelAdd" id="agama-lbl" for="religion">Agama
                                                :</label><br>
                                            <select name="religion" id="agama-lbl" class="inputPegawai form-control"
                                                aria-placeholder="Pilih agama pegawai" required>
                                                <option class="optAgama"> Pilih agama</option>
                                                <option value="islam" class="optAgama"> Islam</option>
                                                <option value="kristen" class="optAgama"> Kristen</option>
                                                <option value="katolik" class="optAgama"> Katolik</option>
                                                <option value="hindu" class="optAgama"> Hindu</option>
                                                <option value="buddha" class="optAgama"> Buddha</option>
                                                <option value="konghucu" class="optAgama"> Konghucu</option>
                                            </select>
                                        </div>
                                        <div class="col-md-6 mt-3">
                                            <label class="statusPerkawinan labelAdd" id="statusPerkawinan-lbl"
                                                for="marriage">Status Perkawinan :</label><br>
                                            <select name="marriage" id="statusPerkawinan-lbl"
                                                class="inputPegawai form-control" aria-placeholder="Pilih status" required>
                                                <option class="optKelamin">Pilih status</option>
                                                <option value="menikah" class="optKelamin">Kawin</option>
                                                <option value="belum-menikah" class="optKelamin">Belum Kawin</option>
                                                <option value="cerai-hidup" class="optKelamin">Cerai Hidup</option>
                                                <option value="cerai-mati" class="optKelamin">Cerai Mati</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6 mt-3">
                                            <label class="email labelAdd" id="email-lbl" for="email">Email</label><br>
                                            <input type="email" class="input-tambah form-control" name="email"
                                                id="email" placeholder="Masukan email pegawai" required>
                                        </div>
                                        <div class="col-md-6 mt-3">
                                            <label class="noTelp labelAdd" id="noTelp-lbl" for="phone_number">Nomor
                                                Telepon</label><br>
                                            <input type="number" class="input-tambah form-control" name="phone_number"
                                                id="noTelp" placeholder="Masukan nomor telepon pegawai" required>
                                        </div>
                                    </div>
                                    <div class="row">
                                        {{-- button --}}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                {{-- Halaman Kedua (DATA KEPEGAWAIAN) --}}
                <div v-show="currentstep == 2">
                    <div class="card">
                        <div class="container-fluid mt-3">
                            <div class="row">
                                <div class="col-md-4">
                                    <!-- CARD -->
                                    <div class="card bg-cardGradient mt-4">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="d-flex justify-content-center">
                                                    <x-icon type="icon-datadiri" />

                                                </div>
                                            </div>
                                        </div>
                                        <!-- CARD BODY -->
                                        <div class="card-body">
                                            <h5 class="card-title addTitle-card">Data Kepegawaian</h5>
                                            <h5 class="card-text addBody-card">Data yang akan dilengkapi oleh Admin, akan
                                                disesuaikan oleh data pegawai masing-masing</h5>
                                        </div>
                                    </div>
                                    <div class="page text-center">
                                        <span class="dot1"></span>
                                        <span class="dotActive"></span>
                                        <span class="dot1"></span>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="row">
                                        <div class="col-md-12 mt-3">
                                            <label class="namalembaga labelAdd" id="namalembaga-lbl"
                                                for="institution">Nama Lembaga / Perusahaan</label><br>
                                            <input type="text" class="input-tambah form-control" name="institution"
                                                id="namalembaga" placeholder="SMK Wikrama Bogor" required >
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6 mt-3">
                                            <label class="idPegawai labelAdd" id="idPegawai-lbl" for="idPegawai">ID
                                                Pegawai
                                                :</label><br>
                                            <input type="text" class="input-tambah form-control" name="idPegawai"
                                                id="idPegawai" placeholder="ID PEGAWAI DI GENERATE OTOMATIS" required
                                                disabled>

                                        </div>
                                        <div class=" col-md-6 mt-3">
                                            <label class="posisi labelAdd" id="posisi-lbl" for="employee_tier">Posisi
                                                Pegawai</label><br>
                                            <input type="text" class="input-tambah form-control" name="employee_tier"
                                                id="posisi" placeholder="Masukan posisi pegawai" required>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 mt-3">
                                            <label class="status labelAdd" id="status-lbl" for="employee_stats">Status
                                                Pegawai
                                                :</label><br>
                                            <input type="text" class="input-tambah form-control" name="employee_stats"
                                                id="status" placeholder="Masukan status pegawai" required>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6 mt-3">
                                            <label class="tglGabung labelAdd" id="tglGabung-lbl" for="join_date">Tanggal
                                                Bergabung :</label><br>
                                            <input type="date" class="input-tambah form-control" name="join_date"
                                                id="tglGabung" required>
                                        </div>
                                        <div class="col-md-6 mt-3">
                                            <label class="tglBerhenti labelAdd" id="tglBerhenti-lbl"
                                                for="stop_date">Tanggal
                                                Berhenti :</label><br>
                                            <input type="date" class="input-tambah form-control" name="stop_date"
                                                id="tglBerhenti">
                                        </div>
                                    </div>


                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                {{-- Halaman Ketiga (REVIEW DATA)  --}}
                <div v-show="currentstep == 3">
                    <div class="card">
                        <div class="container-fluid mt-3">
                            <div class="row">
                                <div class="col-md-4">
                                    <!-- CARD -->
                                    <div class="card bg-cardGradient mt-4">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="d-flex justify-content-center">
                                                    <x-icon type="icon-datadiri" />

                                                </div>
                                            </div>
                                        </div>
                                        <!-- CARD BODY -->
                                        <div class="card-body">
                                            <h5 class="card-title addTitle-card">Review Data</h5>
                                            <h5 class="card-text addBody-card">Review data sebelum di submit</h5>
                                        </div>
                                    </div>
                                    <div class="page text-center">
                                        <span class="dot1"></span>
                                        <span class="dot1"></span>
                                        <span class="dotActive"></span>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <h1 class="hero-text"># Data Diri</h1>
                                    <div class="row">
                                        <div class="col-md-6 mt-3">
                                            <label class="namaDepan label-detail" id="namaDepan-lbl"
                                                for="namaDepan">Nama</label><br>
                                            <p class="value-detail" id="preview-fullname"></p>
                                            <hr>
                                        </div>
                                        <div class="col-md-6 mt-3">
                                            <label class="nik label-detail" id="nik-lbl" for="nik">NIK</label><br>
                                            <p class="value-detail" id="preview-nik"></p>
                                            <hr>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6 mt-3">
                                            <label class="tempat-lahir label-detail" id="tempat-lahir-lbl"
                                                for="tempat-lahir">Tempat Lahir</label><br>
                                            <p class="value-detail">Tangerang</p>
                                            <hr>
                                        </div>
                                        <div class=" col-md-6 mt-3">
                                            <label class="tgl-lahir label-detail" id="tgl-lahir-lbl"
                                                for="tgl-lahir">Tanggal Lahir</label><br>
                                            <p class="value-detail" id="preview-birth_place"></p>
                                            <hr>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6 mt-3">
                                            <label class="alamat label-detail" id="alamat-lbl"
                                                for="alamat">Alamat</label><br>
                                            <p class="value-detail" id="preview-address"></p>
                                            <hr>
                                        </div>
                                        <div class="col-md-6 mt-3">
                                            <label class="jk label-detail" id="jk-lbl" for="jk">Jenis
                                                Kelamin</label><br>
                                                <p class="value-detail" id="preview-gender"></p>
                                            <hr>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6 mt-3">
                                            <label class="agama label-detail" id="agama-lbl" for="agama">Agama
                                                :</label><br>
                                                <p class="value-detail" id="preview-religion"></p>
                                            <hr>
                                        </div>
                                        <div class="col-md-6 mt-3">
                                            <label class="status-perkawinan label-detail" id="status-perkawinan-lbl"
                                                for="status-perkawinan">Status Perkawinan</label><br>
                                                <p class="value-detail" id="preview-mariage"></p>
                                            <hr>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6 mt-3">
                                            <label class="email label-detail" id="email-lbl"
                                                for="email">Email</label><br>
                                                <p class="value-detail" id="preview-email"></p>
                                            <hr>
                                        </div>
                                        <div class="col-md-6 mt-3">
                                            <label class="no-telp labelAdd" id="no-telp-lbl" for="no-telp">Nomor
                                                Telepon</label><br>
                                                <p class="value-detail" id="preview-phone_number"></p>
                                            <hr>
                                        </div>
                                    </div>

                                    {{-- DATA KEPEGAWAIAN --}}
                                    <h1 class="hero-text"># Data Kepegawaian</h1>
                                    <div class="row">
                                        <div class="col-md-12 mt-3">
                                            <label class="lembaga labelAdd" id="lembaga-lbl" for="lembaga">Nama
                                                Lembaga / Perusahaan</label><br>
                                                <p class="value-detail" id="preview-institution"></p>
                                            <hr>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6 mt-3">
                                            <label class="id-pegawai label-detail" id="id-pegawai-lbl"
                                                for="id-pegawai">ID Pegawai</label><br>
                                                <p class="value-detail" id="preview-uuid"></p>
                                            <hr>
                                        </div>
                                        <div class="col-md-6 mt-3">
                                            <label class="posisi labelAdd" id="posisi-lbl" for="posisi">Posisi
                                                Pegawai</label><br>
                                                <p class="value-detail" id="preview-employee_tier"></p>
                                            <hr>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6 mt-3">
                                            <label class="status label-detail" id="status-lbl"
                                                for="status">Status</label><br>
                                                <p class="value-detail" id="preview-employee_stats"></p>
                                            <hr>
                                        </div>
                                        <div class="col-md-6 mt-3">
                                            <label class="lama-bekerja labelAdd" id="lama-bekerja-lbl"
                                                for="lama-bekerja">Lama Bekerja</label><br>
                                                <p class="value-detail" id="preview-lama_kerja"></p>
                                            <hr>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6 mt-3">
                                            <label class="tgl-gabung label-detail" id="tgl-gabung-lbl"
                                                for="tgl-gabung">Tanggal Bergabung</label><br>
                                                <p class="value-detail" id="preview-join_date"></p>
                                            <hr>
                                        </div>
                                        <div class="col-md-6 mt-3">
                                            <label class="tgl-berhenti labelAdd" id="tgl-berhenti-lbl"
                                                for="tgl-berhenti">Tanggal Berhenti</label><br>
                                                <p class="value-detail" id="preview-stop_date"></p>
                                            <hr>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <step v-for="step in steps" :currentstep="currentstep" :key="step.id" :step="step"
                    :stepcount="steps.length" @step-change="stepChanged">
                </step>

                <div class="card-footer">
                    <script type="x-template" id="step-navigation-template">
                    <ol class="step-indicator">
                        <li v-for="step in steps" is="step-navigation-step" :key="step.id" :step="step" :currentstep="currentstep">
                        </li>
                    </ol>
                </script>

                    <script type="x-template" id="step-navigation-step-template">
                    <li :class="indicatorclass">
                        <div class="step"><i :class="step.icon_class"></i></div>
                        <div class="caption hidden-xs hidden-sm">Step <span v-text="step.id"></span>: <span v-text="step.title"></span></div>
                    </li>
                </script>

                    <script type="x-template" id="step-template">
                    <div class="step-wrapper" :class="stepWrapperClass">
                        <button type="button" class="btn btn-danger btn-kembali" @click="lastStep" :disabled="firststep">
                            Kembali
                        </button>
                        <button type="button" class="btn btn-primary btn-perbarui" @click="nextStep" :disabled="laststep" none="laststep">
                            Selanjutnya
                        </button>
                        <button type="submit" class="btn btn-primary btn-perbarui" v-if="laststep">
                            Submit
                        </button>
                    </div>
                </script>
                </div>
            </div>
        </div>
    </form>
@endsection

@section('script')
    @vite('resources/js/admin/teachers/add/script.js')
@endsection
