@extends('layouts.master')
@section('title', 'SIM Absensi Wikrama | Detail Employment')

@section('css')
    @vite('resources/css/admin/teachers/detail/style.css')
@endsection

@section('content')
    <div id="layoutSidenav_content">
        <main>
            <div class="container-fluid">
                <div class="card">
                    <div class="container-fluid mt-3">
                        <div class="row">
                            <div class="col-md-4">
                                <!-- CARD -->
                                <div class="card card-shape mt-4">
                                    <div class="card-header bg-headercard">
                                        <img src="{{ $employee->user->avatar }}" alt="profile_picture"
                                            class="pict-header">
                                    </div>
                                    <div class="card-body body-card">
                                        <h5 class="title-card">{{ $employee->user->fullname }}</h5>
                                        <h5 class="card-subtitle title-card2 text-uppercase">{{ $employee->employee_tier }}
                                        </h5>

                                        <hr>

                                        <!-- CARD BODY -->
                                        <h5 class="card-text title-card2">Status Pegawai <span class="titikdua">:</span>
                                            <strong class="font-bold">{{ $employee->employee_stats }}</strong>
                                        </h5>
                                    </div>

                                </div>

                                @include('_partials.listgroup.listgroup-detail')

                            </div>
                            <div class="col-md-8">
                                <div class="row">
                                    <div class="col-md-12 mt-3">
                                        <label class="namaDepan label-detail" id="namaDepan-lbl" for="namaDepan">Nama
                                            Lembaga / Perusahaan
                                            :</label><br>
                                        <p class="value-detail">{{ $employee->user->employee->institution }}</p>
                                        <hr>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 mt-3">
                                        <label class="idpegawai label-detail" id="idpegawai-lbl" for="idpegawai">ID
                                            Pegawai</label><br>
                                        <p class="value-detail">{{ $employee->user->employee->uuid }}</p>
                                        <hr>
                                    </div>
                                    <div class=" col-md-6 mt-3">
                                        <label class="role label-detail" id="role-lbl" for="role">Posisi
                                            Pegawai</label><br>
                                        <p class="value-detail">{{ $employee->user->employee->employee_tier }}</p>
                                        <hr>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 mt-3">
                                        <label class="statuspegawai label-detail" id="statuspegawai-lbl"
                                            for="statuspegawai">Status Pegawai</label><br>
                                        <p class="value-detail">{{ $employee->user->employee->employee_stats }}</p>
                                        <hr>
                                    </div>
                                    <div class="col-md-6 mt-3">
                                        <label class="lamabekerja label-detail" id="lamabekerja-lbl" for="lamabekerja">Lama
                                            Bekerja</label><br>
                                        <p class="value-detail">{{ $month_date }}</p>
                                        <hr>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 mt-3">
                                        <label class="tglbergabung label-detail" id="tglbergabung-lbl"
                                            for="tglbergabung">Tanggal Bergabung</label><br>
                                        <p class="value-detail">{{ $employee->user->employee->join_date }}</p>
                                        <hr>
                                    </div>
                                    <div class="col-md-6 mt-3">
                                        <label class="tanggalberhenti label-detail" id="tanggalberhenti-lbl"
                                            for="tanggalberhenti">Tanggal Berhenti</label><br>
                                        <p class="value-detail">{{ $employee->user->employee->stop_date ?? '-' }}</p>
                                        <hr>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 mt-3">
                                    </div>
                                    <div class="col-md-6 mt-3">
                                        <a href="{{ route('teachers.edit', ['teacher' => $employee->uuid]) }}" class="btn btn-success btn-edit" style="width: 100%;">Edit
                                            Data</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
    </div>
@endsection

@section('script')
    {{-- @vite('resources/js/admin/teachers/script.js'); --}}
@endsection
