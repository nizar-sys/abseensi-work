@extends('layouts.master')

@section('title', 'SIM Absensi Wikrama | Detail Teacher')

@section('css')
    @vite('resources/css/admin/teachers/detail/style.css')
@endsection

@section('content')
    <div id="layoutSidenav_content">
        <main>
            <div class="container-fluid">
                <div class="card">
                    <div class="container-fluid mt-3">
                        <div class="row">
                            <div class="col-md-4">
                                <!-- CARD -->
                                <div class="card card-shape mt-4">
                                    <div class="card-header bg-headercard">
                                        <img src="{{ $employee->user->avatar }}" alt="profile_picture"
                                            class="pict-header">
                                    </div>
                                    <div class="card-body body-card">
                                        <h5 class="title-card">{{ $employee->user->fullname }}</h5>
                                        <h5 class="card-subtitle title-card2 text-uppercase">{{ $employee->employee_tier }}
                                        </h5>

                                        <hr>

                                        <!-- CARD BODY -->
                                        <h5 class="card-text title-card2">Status Pegawai <span class="titikdua">:</span>
                                            <strong class="font-bold">{{ $employee->employee_stats }}</strong>
                                        </h5>
                                    </div>

                                </div>

                                @include('_partials.listgroup.listgroup-detail', $employee)

                            </div>
                            <div class="col-md-8">
                                <div class="row">
                                    <div class="col-md-6 mt-3">
                                        <label class="namaDepan label-detail" id="namaDepan-lbl" for="namaDepan">Nama
                                            :</label><br>
                                        <p class="value-detail">{{ $employee->user->fullname }}</p>
                                        <hr>
                                    </div>
                                    <div class="col-md-6 mt-3">
                                        <label class="nik label-detail" id="nik-lbl" for="nik">NIK
                                            :</label><br>
                                        <p class="value-detail">{{ $employee->user->personal->nik }}</p>
                                        <hr>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 mt-3">
                                        <label class="tempatlahir label-detail" id="tempatlahir-lbl" for="namaDepan">Tempat
                                            Lahir
                                            :</label><br>
                                        <p class="value-detail">{{ $employee->user->personal->birth_place }}</p>
                                        <hr>
                                    </div>
                                    <div class=" col-md-6 mt-3">
                                        <label class="tgllahir label-detail" id="tgllahir-lbl" for="tgllahir">Tanggal Lahir
                                            :</label><br>
                                        <p class="value-detail">
                                            {{ date('Y-m-d', strtotime($employee->user->personal->birth_date)) }}</p>
                                        <hr>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 mt-3">
                                        <label class="alamat label-detail" id="alamat-lbl" for="alamat">Alamat
                                            :</label><br>
                                        <p class="value-detail">{{ $employee->user->personal->address }}</p>
                                        <hr>
                                    </div>
                                    <div class="col-md-6 mt-3">
                                        <label class="jk label-detail" id="jk-lbl" for="jk">Jenis
                                            Kelamin</label><br>
                                        <p class="value-detail">{{ $employee->user->personal->gender }}</p>
                                        <hr>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 mt-3">
                                        <label class="agama label-detail" id="agama-lbl" for="agama">Agama</label><br>
                                        <p class="value-detail">{{ $employee->user->personal->religion }}</p>
                                        <hr>
                                    </div>
                                    <div class="col-md-6 mt-3">
                                        <label class="statusperkawinan label-detail" id="statusperkawinan-lbl"
                                            for="statusperkawinan">Status Perkawinan</label><br>
                                        <p class="value-detail">{{ $employee->user->personal->marriage }}</p>
                                        <hr>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 mt-3">
                                        <label class="email label-detail" id="email-lbl" for="email">Email</label><br>
                                        <p class="value-detail">{{ $employee->user->email }}</p>
                                        <hr>
                                    </div>
                                    <div class="col-md-6 mt-3">
                                        <label class="notelp label-detail" id="notelp-lbl" for="notelp">Nomor
                                            Telepon</label><br>
                                        <p class="value-detail">{{ $employee->user->personal->phone_number }}</p>
                                        <hr>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 mt-3">
                                    </div>
                                    <div class="col-md-6 mt-3">
                                        <a href="{{ route('teachers.edit', ['teacher' => $employee->uuid]) }}" class="btn btn-success btn-edit" style="width: 100%;">Edit
                                            Data</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
    </div>
@endsection

@section('script')
    {{-- @vite('resources/js/admin/teachers/script.js'); --}}
@endsection
