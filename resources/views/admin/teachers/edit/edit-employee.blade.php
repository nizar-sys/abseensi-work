@extends('layouts.master')
@section('title', 'SIM Absensi Wikrama | Edit Teacher')

@section('css')
    @vite('resources/css/admin/teachers/edit/style.css')
@endsection

@section('content')
    <div id="layoutSidenav_content">
        <form action="{{ route('teachers.update', ['teacher' => $user->employee->uuid]) }}" method="post">
            @csrf
            @method('PUT')
            <main>
                <div class="container-fluid">
                    <div class="card">
                        <div class="container-fluid mt-3">
                            <div class="row">
                                <div class="col-md-4">
                                    <!-- CARD -->
                                    <div class="card mt-4">
                                        <div class="card-header bg-headercard">
                                            <img src="{{ asset('/master/img/Ellipse2.png') }}" alt="profile_picture"
                                                class="pict-header">
                                        </div>
                                        <div class="card-body body-card">
                                            <h5 class="title-card">{{ ucwords($user->fullname) }}</h5>
                                            <h5 class="card-subtitle title-card2">{{ ucwords($user->role) }}</h5>

                                            <hr>

                                            <!-- CARD BODY -->
                                            <h5 class="card-text title-card2">Status Pegawai <span class="titikdua">:</span>
                                                <strong
                                                    class="font-bold">{{ ucwords($user->employee->employee_stats) }}</strong>
                                            </h5>
                                            <h5 class="card-text title-card2">Level Pegawai <span class="titikdua2">:</span>
                                                <strong
                                                    class="font-bold">{{ ucwords($user->employee->employee_tier) }}</strong>
                                            </h5>
                                        </div>
                                    </div>
                                    @include('_partials.listgroup.listgroup-edit')
                                </div>
                                <div class="col-md-8">
                                    <div class="row">
                                        <div class="col-md-12 mt-3">
                                            <label class="namaperusahaan labelAdd" id="namaperusahaan-lbl"
                                                for="institution">Nama Lembaga / Perusahaan</label><br>
                                            <input type="text" class="input-tambah form-control" name="institution"
                                                id="namaperusahaan" placeholder="Masukan nama lembaga Anda"
                                                value="{{ old('institution', ucwords($user->employee->institution)) }}"
                                                required>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6 mt-3">
                                            <label class="idpegawai labelAdd" id="idpegawai-lbl" for="idpegawai">ID
                                                Pegawai</label><br>
                                            <input type="text" class="input-tambah form-control" name="idpegawai"
                                                id="idpegawai" placeholder="ID di generate otomatis"
                                                value="{{ $user->uuid }}" disabled>
                                        </div>
                                        <div class="col-md-6 mt-3">
                                            <label class="posisi labelAdd" id="posisi-lbl" for="employee_tier">Posisi
                                                Pegawai</label><br>
                                            <input type="text" class="input-tambah form-control" name="employee_tier"
                                                id="posisi" placeholder="Masukan posisi Anda"
                                                value="{{ old('employee_tier', ucwords($user->employee->employee_tier)) }}"
                                                required>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6 mt-3">
                                            <label class="status labelAdd" id="status-lbl" for="employee_stats">Status
                                                Pegawai</label><br>
                                            <input type="text" class="input-tambah form-control" name="employee_stats"
                                                id="status" placeholder="Masukan status Anda"
                                                value="{{ old('employee_stats', ucwords($user->employee->employee_stats)) }}"
                                                required>
                                        </div>
                                        <div class="col-md-6 mt-3">
                                            <label class="lamabekerja labelAdd" id="lamabekerja-lbl" for="lamabekerja">Lama
                                                Bekerja</label><br>
                                            <input type="text" class="input-tambah form-control" name="lamabekerja"
                                                id="lamabekerja" placeholder="Masukan lama bekerja Anda"
                                                value="{{ old('lamabekerja', ucwords($user->employee->join_date)) }}"
                                                required disabled>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6 mt-3">
                                            <label class="tglbergabung labelAdd" id="tglbergabung-lbl"
                                                for="join_date">Tanggal Bergabung</label><br>
                                            <input type="date" class="input-tambah form-control" name="join_date"
                                                id="tglbergabung"
                                                value="{{ old('join_date', ucwords($user->employee->join_date)) }}"
                                                required disabled>
                                        </div>
                                        <div class="col-md-6 mt-3">
                                            <label class="tglberhenti labelAdd" id="tglberhenti-lbl" for="stop_date">Tanggal
                                                Berhenti</label><br>
                                            <input type="date" class="input-tambah form-control" name="stop_date"
                                                id="tglberhenti"
                                                value="{{ old('stop_date', ucwords($user->employee->stop_date)) }}"
                                                required disabled>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6 mt-3">
                                            <!-- <a href="/detail-data-diri" class="btn btn-danger btn-kembali" style="width: 100%;">Kembali</a> -->
                                            <a href="/admin/teachers" class="btn btn-danger btn-kembali"
                                                style="width: 100%;">Kembali</a>
                                        </div>
                                        <div class="col-md-6 mt-3">
                                            <button type="submit" class="btn btn-primary btn-perbarui"
                                                style="width: 100%;">Perbarui</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </main>
        </form>
    </div>
@endsection

@section('script')
    @vite('resources/js/admin/teachers/edit/script.js')
@endsection
