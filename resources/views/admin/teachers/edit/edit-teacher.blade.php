@extends('layouts.master')
@section('title', 'SIM Absensi Wikrama | Edit Teacher')

@section('css')
    @vite('resources/css/admin/teachers/edit/style.css')
@endsection

@section('content')
    <div id="layoutSidenav_content">
        <form action="{{ route('teachers.update', ['teacher' => $user->employee->uuid]) }}" method="post">
            @csrf
            @method('PUT')
            <main>
                <div class="container-fluid">
                    <div class="card">
                        <div class="container-fluid mt-3">
                            <div class="row">
                                <div class="col-md-4">
                                    <!-- CARD -->
                                    <div class="card mt-4">
                                        <div class="card-header bg-headercard">
                                            <img src="{{ asset('/master/img/Ellipse2.png') }}" alt="profile_picture"
                                                class="pict-header">
                                        </div>
                                        <div class="card-body body-card">
                                            <h5 class="title-card">{{ $user->fullname }}</h5>
                                            <h5 class="card-subtitle title-card2">{{ $user->role }}</h5>

                                            <hr>

                                            <!-- CARD BODY -->
                                            <h5 class="card-text title-card2">Status Pegawai <span class="titikdua">:</span>
                                                <strong
                                                    class="font-bold">{{ ucwords($user->employee->employee_stats) }}</strong>
                                            </h5>
                                            <h5 class="card-text title-card2">Level Pegawai <span class="titikdua2">:</span>
                                                <strong
                                                    class="font-bold">{{ ucwords($user->employee->employee_tier) }}</strong>
                                            </h5>
                                        </div>
                                    </div>
                                    @include('_partials.listgroup.listgroup-edit', $user)
                                </div>
                                <div class="col-md-8">
                                    <div class="row">
                                        <div class="col-md-6 mt-3">
                                            <label class="nama labelAdd" id="nama-lbl" for="fullname">Nama</label><br>
                                            <input type="text" class="input-tambah form-control" name="fullname"
                                                id="nama" placeholder="Masukan nama anda"
                                                value="{{ old('fullname', $user->fullname) }}" required>
                                        </div>
                                        <div class="col-md-6 mt-3">
                                            <label class="nik labelAdd" id="nik-lbl" for="nik">NIK</label><br>
                                            <input type="text" class="input-tambah form-control" name="nik"
                                                id="nik" placeholder="Masukan NIK Anda"
                                                value="{{ old('nik', $user->personal->nik) }}" required>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6 mt-3">
                                            <label class="tempatlahir labelAdd" id="tempatlahir-lbl"
                                                for="birth_place">Tempat
                                                Lahir
                                                :</label><br>
                                            <input type="text" class="input-tambah form-control" name="birth_place"
                                                id="tempatlahir" placeholder="Masukan tempat lahir Anda"
                                                value="{{ old('birth_place', $user->personal->birth_place) }}" required>
                                        </div>
                                        <div class="col-md-6 mt-3">
                                            <label class="tglLahir labelAdd" id="tglLahir-lbl" for="birth_date">Tanggal
                                                Lahir
                                                :</label><br>
                                            <input type="date" class="input-tambah form-control" name="birth_date"
                                                id="tglLahir" placeholder="Masukan tanggal lahir Anda"
                                                value="{{ old('birth_date', $user->personal->birth_date) }}" required>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6 mt-3">
                                            <label class="alamat labelAdd" id="alamat-lbl" for="address">Alamat</label><br>
                                            <input type="text" class="input-tambah form-control" name="address"
                                                id="alamat" placeholder="Masukan alamat Anda"
                                                value="{{ old('address', $user->personal->address) }}" required>
                                        </div>
                                        <div class="col-md-6 mt-3">
                                            <label class="jenisKelamin labelAdd" id="jenisKelamin-lbl" for="gender">Jenis
                                                Kelamin :</label><br>
                                            <select name="gender" id="jenisKelamin-lbl" class="inputPegawai form-control"
                                                aria-placeholder="Pilih Jenis Kelamin">
                                                <option class="optKelamin"> Pilih jenis kelamin pegawai</option>
                                                @php
                                                    $genders = ['male', 'female'];
                                                @endphp
                                                @foreach ($genders as $gender)
                                                    <option value="{{ $gender }}" class="optKelamin"
                                                        {{ $gender == $user->personal->gender ? 'selected' : '' }}>
                                                        {{ $gender == 'male' ? 'Laki-laki' : 'Perempuan' }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6 mt-3">
                                            <label class="agama labelAdd" id="agama-lbl" for="religion">Agama
                                                :</label><br>
                                            <select name="religion" id="agama-lbl" class="inputPegawai form-control"
                                                aria-placeholder="Pilih agama pegawai" required>
                                                <option class="optAgama" value=""> Pilih agama</option>
                                                @php
                                                    $religions = ['islam', 'kristen', 'katolik', 'hindu', 'buddha', 'konghucu'];
                                                @endphp
                                                @foreach ($religions as $religion)
                                                    <option value="{{ $religion }}" class="optAgama"
                                                        {{ $religion == $user->personal->religion ? 'selected' : '' }}>
                                                        {{ ucwords($religion) }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-md-6 mt-3">
                                            <label class="statusPerkawinan labelAdd" id="statusPerkawinan-lbl"
                                                for="marriage">Status Perkawinan :</label><br>
                                            <select name="marriage" id="statusPerkawinan-lbl"
                                                class="inputPegawai form-control" aria-placeholder="Pilih status" required>
                                                <option class="optKelamin" value="">Pilih status</option>
                                                @php
                                                    $marriages = ['menikah', 'belum menikah', 'cerai hidup', 'cerai mati'];
                                                @endphp
                                                @foreach ($marriages as $marriage)
                                                    <option value="{{ $marriage }}" class="optKelamin"
                                                        {{ $marriage == $user->personal->marriage ? 'selected' : '' }}>
                                                        {{ ucwords($marriage) }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6 mt-3">
                                            <label class="email labelAdd" id="email-lbl" for="email">Email</label><br>
                                            <input type="text" class="input-tambah form-control" name="email"
                                                id="email" placeholder="Masukan email Anda"
                                                value="{{ $user->email }}" required>
                                        </div>

                                        <div class="col-md-6 mt-3">
                                            <label class="noTelp labelAdd" id="noTelp-lbl" for="phone_number">Nomor
                                                Telepon</label><br>
                                            <input type="text" class="input-tambah form-control" name="phone_number"
                                                id="noTelp" placeholder="Masukan nomor telepon Anda"
                                                value="{{ $user->personal->phone_number }}" required>
                                        </div>

                                    </div>
                                    <div class="row">
                                        <div class="col-md-6 mt-3">
                                            <!-- <a href="/detail-data-diri" class="btn btn-danger btn-kembali" style="width: 100%;">Kembali</a> -->
                                            <a href="/admin/teachers" class="btn btn-danger btn-kembali"
                                                style="width: 100%;">Kembali</a>
                                        </div>
                                        <div class="col-md-6 mt-3">
                                            <button type="submit" class="btn btn-primary btn-perbarui"
                                                style="width: 100%;">Perbarui</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </main>
        </form>
    </div>
@endsection

@section('script')
    @vite('resources/js/admin/teachers/edit/script.js')
@endsection
