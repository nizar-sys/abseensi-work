@extends('layouts.master')
@section('title', 'SIM Absensi Wikrama | Teachers')

@section('css')
    @vite('resources/css/admin/teachers/teacher_list.css')
@endsection

@section('content')
    <div id="layoutSidenav_content">
        <main>
            <div class="container-fluid px-4">
                <div class="row">
                    <div class="col">

                        <div class="d-flex flex-row-reverse mb-3">
                            <button type="button" class="btn btnExp btn-primary eksportdata">Eksport Data</button>
                        </div>

                        <div class="card mb-4">
                            <div class="card-header">
                                <i class="fas fa-table me-1"></i>
                                Daftar Pegawai
                            </div>


                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table" id="datatablesSimple">
                                        <thead>
                                            <tr>
                                                <th>
                                                    <svg width="15" height="15" viewBox="0 0 15 15" fill="none"
                                                        xmlns="http://www.w3.org/2000/svg">
                                                        <path
                                                            d="M13.3333 0H1.66667C0.75 0 0 0.75 0 1.66667V13.3333C0 14.25 0.75 15 1.66667 15H13.3333C14.25 15 15 14.25 15 13.3333V1.66667C15 0.75 14.25 0 13.3333 0ZM13.3333 13.3333H1.66667V1.66667H13.3333V13.3333ZM3.33333 6.66667H11.6667V8.33333H3.33333V6.66667Z"
                                                            fill="#AAB2BB" />
                                                    </svg>
                                                </th>
                                                <th>Nama Pegawai</th>
                                                <th>Status Pegawai</th>
                                                <th>Posisi Pekerjaan</th>
                                                <th>Aksi</th>
                                            </tr>
                                        </thead>

                                        <tbody>
                                            @foreach ($employee_list as $employee)
                                                <tr>
                                                    <td><img src="{{ $employee->user->avatar }}" width="60"></td>
                                                    <td>
                                                        <a href={{ route('teachers.show', ['teacher' => $employee->uuid]) }}
                                                            class="rowDetail">
                                                            <div class="col rowName">{{ $employee->user->fullname }}</div>
                                                            <div class="col rowName">
                                                                <small>{{ $employee->user->personal->nik }}</small>
                                                            </div>
                                                        </a>
                                                    </td>
                                                    <td>
                                                        <span
                                                            class="badge bg-{{ $employee->employee_stats }}">{{ $employee->employee_stats }}</span>
                                                    </td>
                                                    <td>{{ $employee->employee_tier }}</td>
                                                    <td class="btn-action d-flex">
                                                        <div class="delete-employee btn btn-sm btn-danger"
                                                            data-id="{{ $employee->user->uuid }}">
                                                            Hapus
                                                        </div>
                                                        <a
                                                            href="{{ route('teachers.edit', ['teacher' => $employee->uuid]) }}">
                                                            <x-icon type="icon-edit" />
                                                        </a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </main>
    </div>
@endsection

@section('script')
    <script>
        var deleteUrl = "{{ url('/admin/teachers/') }}"
    </script>
    @vite('resources/js/admin/teachers/script.js')
@endsection
