@extends('layouts.master')
@section('title', 'SIM Absensi Wikrama | Detail Timeoff')

@section('css')
    @vite('resources/css/admin/timeoff/detail/style.css')
@endsection

@section('content')
    <div id="layoutSidenav_content">
        <main>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-2 col-md-2 col-xl-3">
                        <p class="jud1">Detail Time Off</p>
                        <p class="jud2">Detail data Time Off pegawai</p>
                    </div>
                    <div class="col mt-4 mr-4 text-end">
                        <a href="/admin/timeoff" class="btn btn-kembali">Kembali</a>
                    </div>
                </div>
                <div class="dataGroup row">
                    <div class="col-sm-12 col-md-6 col-lg-3 mt-5 data">
                        <label class="lableDetail">Nama</label>
                        <h4 class=" valueDetail mt-2">Xander mount</h4>
                        <hr style="width: 300px; margin-left: 5px;">
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-3 mt-5">
                        <label class="lableDetail">Id Pegawai</label>
                        <h4 class="valueDetail mt-2">12007787</h4>
                        <hr style="width: 300px; margin-left: 5px;">
                    </div>
                </div>
                <div class="dataGroup row">
                    <div class="col-sm-12 col-md-6 col-lg-3 mt-2 data">
                        <label class="lableDetail">Tanggal Pengajuan</label>
                        <h4 class=" valueDetail mt-2 dateLocalDay"><?= date('d F Y') ?></h4>
                        <hr style="width: 300px; margin-left: 5px;">
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-3 mt-2">
                        <label class="lableDetail">Waktu Pengajuan</label>
                        <h4 class="valueDetail mt-2">07:00</h4>
                        <hr style="width: 300px; margin-left: 5px;">
                    </div>
                </div>
                <div class="dataGroup row">
                    <div class="col-sm-12 col-md-6 col-lg-3 mt-2 data">
                        <label class="lableDetail">Time Off</label>
                        <h4 class=" valueDetail mt-2">Dinas Luar</h4>
                        <hr style="width: 300px; margin-left: 5px;">
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-3 mt-2">
                        <label class="lableDetail">Jenis Time Off</label>
                        <h4 class="valueDetail mt-2">Dinas Luar</h4>
                        <hr style="width: 300px; margin-left: 5px;">
                    </div>
                </div>

                <div class="dataGroup row">
                    <div class="col-sm-12 col-md-6 col-lg-3 mt-2 data">
                        <label class="lableDetail">Tanggal Mulai</label>
                        <h4 class=" valueDetail mt-2">3 Agustus 2022</h4>
                        <hr style="width: 300px; margin-left: 5px;">

                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-3 mt-2">
                        <label class="lableDetail">Tanggal Selesai</label>
                        <h4 class="valueDetail mt-2">5 Agustus 2022</h4>
                        <hr style="width: 300px; margin-left: 5px;">

                    </div>
                </div>
                <div class="dataGroup row">
                    <div class="col-sm-12 col-md-6 col-lg-3 mt-2 data">
                        <label class="lableDetail mt">Keterangan</label>
                        <h4 class="valueDetail mt-2">Liburan keluarga</h4>
                        <hr style="width: 735px; margin-left: 5px;">
                    </div>
                </div>
                <div class="dataGroup row">
                    <div class="col-sm-12 col-md-6 col-lg-5 mt-4 text-end">
                        <button class="btn btn-success btn-stj">Setuju</button>
                        <button type="button" class="btn btn-danger btn-tlk" data-toggle="modal"
                            data-target="#exampleModal">Tolak</button>
                    </div>
                </div>
            </div>
        </main>
    </div>

    <!-- Modal Tolak detail timeoff -->
    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="col">
                        <h5 class="modal-title text-center" id="modalToggleLabel">Alasan Penolakan :</h5>
                    </div>
                    <!-- <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button> -->
                </div>
                <div class="modal-body">
                    <form>
                        <div class="mb-3">
                            <label for="message-text" class="col-form-label">Alasan :</label>
                            <textarea class="form-control" id="message-text"></textarea>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary btnKembaliModal" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary btnModal" onclick="alertTolak()">Send message</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    @vite('resources/js/admin/timeoff/detail/script.js')
@endsection
