@extends('layouts.master')
@section('title', 'SIM Absensi | Profile')

@section('css')
    @vite('resources/css/admin/profile/style.css')
@endsection

@section('content')
    <div id="layoutSidenav_content">
        <main>
            <div class="container-fluid">
                <div class="card full-card">
                    <div class="container-fluid mt-3">
                        <div class="row">
                            <div class="col-md-4">
                                <!-- CARD -->
                                <div class="side-infosaya">
                                    <ul class="list-group mt-3">
                                        <a href="/profile" class="list-group-item aktif listgroup-container">Akun
                                            & Profil</a>
                                    </ul>
                                </div>
                            </div>
                            <!-- INFO AKUN DAN PROFIL -->
                            <div class="col-md-8 garis">
                                <h1 class="judul-info">Akun & Profile</h1>

                                <h5 class="content-info">Informasi Profil</h5>
                                <hr>
                                <h5 class="content-info">Informasi Profil ini akan tampil sebagai user akun anda</h5>

                                <div class="positioning-card">
                                    <div class="card adj-card">
                                        <div class="row">
                                            <div class="col-md-2">
                                                <img src="{{ asset('/assets/img/buletan-abu.png') }}" alt=""
                                                    class="adj-img">
                                            </div>
                                            <div class="col-md-3">
                                                <h5 class="info-nama">Nama</h5>
                                                <h5 class="info-nama2">Eko Setyono Wibowo</h5>
                                            </div>
                                            <div class="col-md-4">
                                                <h5 class="info-nama">Email</h5>
                                                <h5 class="info-nama2">bowo1120@gmail.com</h5>
                                            </div>
                                            <div class="col-md-3">
                                                <a href="#" class="btn btn-ubah">Ubah</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <!-- INFO PERUSAHAAN -->
                                <h5 class="content-info mt-5">Informasi Sekolah</h5>
                                <hr>
                                <h5 class="content-info">Jika Anda memiliki Wikrama Device, informasi Sekolah Anda akan
                                    muncul</h5>

                                <div class="positioning-card2">
                                    <div class="card adj-card">
                                        <div class="row">
                                            <div class="col-md-2">
                                                <img src="{{ asset('assets/img/wikrama-logo.png') }}" alt=""
                                                    class="adj-img2">
                                            </div>
                                            <div class="col-md-3 mt-2">
                                                <h5 class="info-nama">Nama Sekolah</h5>
                                                <h5 class="info-nama2">SMK Wikrama Bogor</h5>
                                            </div>
                                            <div class="col-md-4 mt-2">
                                                <h5 class="info-nama">Yayasan</h5>
                                                <h5 class="info-nama2">Yayasan Prawitama</h5>
                                            </div>
                                            <div class="col-md-3 mt-2">
                                                <a href="#" class="btn btn-ubah2">Ubah</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
    </div>
@endsection
