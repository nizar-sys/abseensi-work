<?php

use App\Http\Controllers\Admin\PresencesController;
use App\Http\Controllers\Admin\TeachersController;
use App\Http\Controllers\Admin\TimeoffController;
use Illuminate\Support\Facades\Route;

Route::get('/', [TeachersController::class, 'index'])->name('dashboard');
Route::get('/detail-mini-dashboard', [PresencesController::class, 'miniDashboard'])->name('mini-dashboard');

Route::get('/teachers/detail/{uuid}', [TeachersController::class, 'detail'])->name('teachers.detail');
Route::get('/teachers/detail/{uuid}/edit', [TeachersController::class, 'detailEdit'])->name('teachers.detail-edit');
Route::resource('teachers', TeachersController::class);

Route::resource('presences', PresencesController::class);
Route::resource('timeoff', TimeoffController::class);
