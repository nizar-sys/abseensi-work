<?php

use App\Http\Controllers\Api\Admin\AttendanceController;
use App\Http\Controllers\Api\Admin\SubmissionController;
use App\Http\Controllers\Api\Admin\TeachersController;
use App\Http\Controllers\Api\AuthController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/', function () {
    return response()->json([
        'response' => 'success',
        'message' => "Selamat datang di aplikasi SIM Abseensi SMK Wikrama Bogor.",
        'author' => "Gen 25 RPL"
    ]);
});

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group([
    'prefix' => 'auth'
], function () {
    Route::post('login', [AuthController::class, 'login'])->name('login');
    Route::group([
        'middleware' => 'auth:sanctum'
    ], function () {
        Route::get('user', [AuthController::class, 'user']);
        Route::post('logout', [AuthController::class, 'logout']);
    });
});

Route::middleware('auth:sanctum')->group(function(){

    // employee, admin, teachers
    Route::resource('teachers', TeachersController::class);

    // submissions
    Route::resource('submissions', SubmissionController::class);

    // attendances
    Route::resource('attendances', AttendanceController::class);
});
