import { defineConfig } from 'vite';
import laravel from 'laravel-vite-plugin';

export default defineConfig({
    plugins: [
        laravel({
            input: [
                'resources/css/app.css',
                'resources/js/app.js',
                'resources/js/master/script.js',
                'resources/js/dashboard/datetime_dashboard.js',
                // TEACHERS
                'resources/js/admin/teachers/script.js',
                'resources/css/admin/teachers/teacher_list.css',

                // DETAIL TEACHER
                'resources/css/admin/teachers/detail/style.css',

                // PRESENCES
                'resources/css/admin/presences/style.css',
                'resources/js/admin/presences/script.js',

                // TIME OFF
                'resources/css/admin/timeoff/style.css',
                'resources/css/admin/timeoff/table.css',
                'resources/js/admin/timeoff/script.js',

                // ADD TEACHERS
                'resources/css/admin/teachers/add/style.css',
                'resources/css/admin/teachers/add/wizard.css',
                'resources/js/admin/teachers/add/script.js'


            ],
            refresh: true,
        }),
    ],
});
